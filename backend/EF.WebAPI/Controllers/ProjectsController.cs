﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using EF.Common.DTO.Project;
using EF.Common.DTO.QueryResults;
using EF.BLL.Interfaces;
using System.Threading.Tasks;

namespace EF.WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;
        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }
        [HttpGet]
        public async Task<IEnumerable<ProjectDTO>> GetAsync()
        {
            return await _projectService.GetAllAsync();
        }
        [HttpGet("{id}", Name = "ProjectGet")]
        public async Task<ActionResult<ProjectDTO>> GetAsync(int id)
        {
            return Ok(await _projectService.GetAsync(id));
        }
        [HttpPost]
        public async Task<ActionResult<ProjectDTO>> PostAsync([FromBody] ProjectCreateDTO project)
        {
            var newProject = await _projectService.PostAsync(project);
            return CreatedAtRoute("ProjectGet", new { newProject.Id }, newProject);
        }
        [HttpPut]
        public async Task<ActionResult<ProjectDTO>> PutAsync([FromBody] ProjectUpdateDTO project)
        {
            
            var updateProject = await _projectService.UpdateAsync(project);
            return CreatedAtRoute("ProjectGet", new { updateProject.Id }, updateProject);
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            await _projectService.DeleteAsync(id);
            return NoContent();
        }
        [HttpGet("TasksCountByUser/{id}")]
        public async Task<ActionResult<IEnumerable<TaskCountDTO>>> GetTasksCountByUserAsync(int id)
        {
            return Ok(await _projectService.GetTasksCountByUserAsync(id));
        }
        [HttpGet("UserStatistics/{id}")]
        public async Task<ActionResult<UserStatisticDTO>> GetUserStatisticsAsync(int id)
        {
            return Ok(await _projectService.GetUserStatisticsAsync(id));
        }
        [HttpGet("ProjectStatistics")]
        public async Task<IEnumerable<ProjectStatisticDTO>> GetProjectStatisticsAsync()
        {
            return await _projectService.GetProjectStatisticsAsync();
        }

    }
}
