﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using EF.Common.DTO.QueryResults;
using EF.Common.DTO.Team;
using EF.BLL.Interfaces;
using System.Threading.Tasks;

namespace EF.WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;
        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }
        [HttpGet]
        public async Task<IEnumerable<TeamDTO>> GetAsync()
        {
            return await _teamService.GetAllAsync();
        }
        [HttpGet("{id}", Name = "TeamGet")]
        public async Task<ActionResult<TeamDTO>> GetAsync(int id)
        {
            return Ok(await _teamService.GetAsync(id));
        }
        [HttpPost]
        public async Task<ActionResult<TeamDTO>> PostAsync([FromBody] TeamCreateDTO team)
        {
            var newTeam = await _teamService.PostAsync(team);
            return CreatedAtRoute("TeamGet", new { newTeam.Id }, newTeam);
        }
        [HttpPut]
        public async Task<ActionResult<TeamDTO>> PutAsync([FromBody] TeamUpdateDTO team)
        {
            var updateTeam = await _teamService.UpdateAsync(team);
            return CreatedAtRoute("TeamGet", new { updateTeam.Id }, updateTeam);
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            await _teamService.DeleteAsync(id);
            return NoContent();
        }
        [HttpGet("TeamMembers")]
        public async Task<ActionResult<IEnumerable<TeamMembersDTO>>> GetTeamMembersAsync()
        {
            return Ok(await _teamService.GetTeamMembersAsync());
        }
    }
}
