﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EF.DAL.Models
{
    public class Project : Entity
    {
        [MaxLength(100)]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public int? AuthorId { get; set; }
        public int? TeamId { get; set; }
        [NotMapped]
        public IEnumerable<ProjectTask> Tasks { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }
        public Project() : base() 
        {
            CreatedAt = DateTime.Now;
        }
    }
}
