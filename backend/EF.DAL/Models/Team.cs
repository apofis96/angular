﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EF.DAL.Models
{
    public class Team : Entity
    {
        [MaxLength(100)]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public Team() : base() 
        {
            CreatedAt = DateTime.Now;
        }
    }
}
