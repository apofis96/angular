﻿namespace EF.Common
{
    public enum TaskState
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
