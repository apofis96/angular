﻿using EF.Common.DTO.Project;

namespace EF.Common.DTO.QueryResults
{
    public class TaskCountDTO
    {
        public ProjectDTO Key { get; set; }
        public int Value { get; set; }

    }
}
