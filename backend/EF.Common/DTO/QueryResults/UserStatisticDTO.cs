﻿using EF.Common.DTO.User;
using EF.Common.DTO.ProjectTask;
using EF.Common.DTO.Project;
namespace EF.Common.DTO.QueryResults
{
    public class UserStatisticDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int LastProjectTasksCount { get; set; }
        public int NotFinishedTasksCount { get; set; }
        public ProjectTaskDTO LongestTask { get; set; }

    }
}
