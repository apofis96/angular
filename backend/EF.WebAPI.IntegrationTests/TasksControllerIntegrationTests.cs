using Xunit;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net;
using System.Text.Json;
using EF.Common.DTO.ProjectTask;
using System.Collections.Generic;
using System.Linq;

namespace EF.WebAPI.IntegrationTests
{
    public class TasksControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        static JsonSerializerOptions options = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true,
            WriteIndented = true,
        };
        public TasksControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task DeleteTask_WhenTaskNotExist_ThanResponseCode404()
        {
            var httpResponse = await _client.DeleteAsync("/Tasks/42");

            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }

        [Fact]
        public async Task DeleteTask_WhenTaskExist_ThanResponseCode204()
        {
            var httpResponse = await _client.DeleteAsync("/Tasks/10");

            Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);
          
        }
        [Fact]
        public async Task GetNotFinishedTasks_WhenUserNotExist_ThanResponseCode404()
        {
            var httpResponse = await _client.GetAsync("/Tasks/NotFinishedTasks/42");

            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }
        [Fact]
        public async Task GetNotFinishedTasks_WhenUserExistAndHaveTwoTasks_ThanResponseCode200AndListLength2()
        {
            var httpResponse = await _client.GetAsync("/Tasks/NotFinishedTasks/5");

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            var tasks = JsonSerializer.Deserialize<IEnumerable<ProjectTaskDTO>>(await httpResponse.Content.ReadAsStringAsync(), options);
            Assert.Equal(2, tasks.Count());
        }
    }
}
