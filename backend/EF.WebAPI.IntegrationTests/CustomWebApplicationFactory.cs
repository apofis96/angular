﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Project_Structure.DAL.Context;
using System.Linq;

namespace EF.WebAPI.IntegrationTests
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            
            builder.ConfigureTestServices(services =>
            {
                var descriptor = services.SingleOrDefault(
                    d => d.ServiceType == typeof(DbContextOptions<EFContext>));
                if (descriptor != null)
                {
                    services.Remove(descriptor);
                }
                
                var serviceDescriptor = services.Where(descriptor => descriptor.ImplementationType == typeof(EFContext)).ToList();
                foreach(var ser in serviceDescriptor)
                    services.Remove(ser);
                services.AddDbContext<DbContext, TestingContext>(options =>
                {
                    options.UseInMemoryDatabase("InMemoryDbForTesting");
                }, ServiceLifetime.Transient);
                var sp = services.BuildServiceProvider();
                using (var scope = sp.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var appDb = scopedServices.GetRequiredService<DbContext>();
                    appDb.Database.EnsureCreated();
                }
            });
        }
    } 
}
