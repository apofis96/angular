﻿using EF.BLL.Interfaces;
using EF.DAL.Interfaces;
using EF.DAL.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EF.BLL.Services
{
    public class ExistService : IExistService
    {
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<ProjectTask> _taskRepository;
        private readonly IRepository<Project> _projectRepository;
        private readonly IRepository<Team> _teamRepository;
        public ExistService(IRepository<User> userRepository, IRepository<ProjectTask> taskRepository, IRepository<Project> projectRepository, IRepository<Team> teamRepository)
        {
            _userRepository = userRepository;
            _taskRepository = taskRepository;
            _projectRepository = projectRepository;
            _teamRepository = teamRepository;
        }
        public async Task ProjectIsExistOrExeptionAsync(int id)
        {
            if ((await _projectRepository.GetAsync(IRepository<Project>.FilterById(id))).FirstOrDefault() == null)
                throw new InvalidOperationException("Project not found");
        }
        public async Task ProjectTaskIsExistOrExeptionAsync(int id)
        {
            if ((await _taskRepository.GetAsync(IRepository<ProjectTask>.FilterById(id))).FirstOrDefault() == null)
                throw new InvalidOperationException("Task not found");
        }
        public async Task TeamIsExistOrExeptionAsync(int id)
        {
            if ((await _teamRepository.GetAsync(IRepository<Team>.FilterById(id))).FirstOrDefault() == null)
                throw new InvalidOperationException("Team not found");
        }
        public async Task UserIsExistOrExeptionAsync(int id)
        {
            if ((await _userRepository.GetAsync(IRepository<User>.FilterById(id))).FirstOrDefault() == null)
                throw new InvalidOperationException("User not found");
        }
    }
}
