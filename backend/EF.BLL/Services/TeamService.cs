﻿using AutoMapper;
using EF.Common.DTO.Team;
using EF.Common.DTO.QueryResults;
using EF.Common.DTO.User;
using EF.BLL.Interfaces;
using EF.DAL.Interfaces;
using EF.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EF.BLL.Services
{
    public class TeamService : ITeamService
    {
        private readonly IRepository<Team> _teamRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IExistService _existService;
        private readonly IMapper _mapper;
        public TeamService(IRepository<Team> teamRepository, IRepository<User> userRepository, IMapper mapper, IExistService existService)
        {
            _teamRepository = teamRepository;
            _userRepository = userRepository;
            _existService = existService;
            _mapper = mapper;
        }
        public async Task<IEnumerable<TeamDTO>> GetAllAsync()
        {
            var teams = await _teamRepository.GetAsync();
            return _mapper.Map<IEnumerable<TeamDTO>>(teams);
        }
        public async Task<TeamDTO> GetAsync(int id)
        {
            await _existService.TeamIsExistOrExeptionAsync(id);
            var team = (await _teamRepository.GetAsync(IRepository<Team>.FilterById(id))).FirstOrDefault();
            return _mapper.Map<TeamDTO>(team);
        }
        public async Task<TeamDTO> PostAsync(TeamCreateDTO team)
        {
            var newTeam = _mapper.Map<Team>(team);
            await _teamRepository.CreateAsync(newTeam);
            return await GetAsync(newTeam.Id);
        }
        public async Task<TeamDTO> UpdateAsync(TeamUpdateDTO updateTeam)
        {
            await _existService.TeamIsExistOrExeptionAsync(updateTeam.Id.Value);
            var team = (await _teamRepository.GetAsync(IRepository<Team>.FilterById(updateTeam.Id.Value))).FirstOrDefault();
            team.Name = updateTeam.Name;
            team.Description = updateTeam.Description;
            await _teamRepository.UpdateAsync(team);
            return await GetAsync(updateTeam.Id.Value);
        }
        public async Task DeleteAsync(int id)
        {
            await _existService.TeamIsExistOrExeptionAsync(id);
            await _teamRepository.DeleteAsync(id);
        }
        public async Task<IEnumerable<TeamMembersDTO>> GetTeamMembersAsync()
        {
            var taskTeams = GetAllAsync();
            var taskUsers = _userRepository.GetAsync();
            await Task.WhenAll(taskTeams, taskUsers);
            return (await taskTeams).Join((await taskUsers)
                .OrderByDescending(u => u.RegisteredAt).GroupBy(u => u.TeamId),
                t => t.Id, u => u.Key, (t, u) => new TeamMembersDTO
                {
                    Id = t.Id,
                    Name = t.Name,
                    Members = _mapper.Map<IEnumerable<UserDTO>>(u)
                }).Where(t => t.Members.All(m => (DateTime.Today.Year - m.Birthday.Year) > 10));
        }
    }
}
