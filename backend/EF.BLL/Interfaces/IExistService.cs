﻿using System.Threading.Tasks;

namespace EF.BLL.Interfaces
{
    public interface IExistService
    {
        Task UserIsExistOrExeptionAsync(int id);
        Task ProjectIsExistOrExeptionAsync(int id);
        Task ProjectTaskIsExistOrExeptionAsync(int id);
        Task TeamIsExistOrExeptionAsync(int id);
    }
}
