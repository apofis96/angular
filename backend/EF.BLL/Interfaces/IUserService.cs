﻿using EF.Common.DTO.QueryResults;
using EF.Common.DTO.User;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EF.BLL.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<UserDTO>> GetAllAsync();
        Task<UserDTO> GetAsync(int id);
        Task<UserDTO> PostAsync(UserCreateDTO user);
        Task<UserDTO> UpdateAsync(UserUpdateDTO user);
        Task DeleteAsync(int id);
        Task<IEnumerable<UserTasksDTO>> GetUserTasksAsync();
    }
}
