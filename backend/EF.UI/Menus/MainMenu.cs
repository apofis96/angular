﻿using EF.Common.DTO.ProjectTask;
using EF.Common.DTO.QueryResults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace EF.UI.Menus
{
    public class MainMenu
    {
        static Lazy<TaskMenu> taskMenu;
        public static async Task Menu(HttpClient client, JsonSerializerOptions options)
        {
            taskMenu = new Lazy<TaskMenu>(() => new TaskMenu(client, options));
            string warning = "Sequence output is limited to three objects!";
            string pause = "Press any key to continue";
            string menuItems = @"1 : Task_1.GetTasksCountByUser
2 : Task_2.GetTasksByUserList
3 : Task_3.GetFinishedTasksList
4 : Task_4.GetTeamMembersList
5 : Task_5.GetUserTasksList
6 : Task_6.GetUserStatistics
7 : Task_7.GetProjectStatisticsList
8 : +MarkRandomTaskWithDelay
a : UserMenu
b : TeamMenu
c : TaskMenu
d : ProjectMenu
0 : Exit.";
            Console.WriteLine(menuItems);
            bool loop = true;
            while (loop)
            {
                try
                {
                    Console.WriteLine("Main menu input: ");
                    char menu = Console.ReadKey().KeyChar;
                    Console.WriteLine("\n================================");
                    switch (menu)
                    {
                        case '1':
                            Console.WriteLine(warning);
                            Console.WriteLine(JsonSerializer.Serialize((await JsonSerializer.DeserializeAsync<IEnumerable<TaskCountDTO>>(await client.GetStreamAsync("Projects/TasksCountByUser/" + MenuInputs.IdInput()), options)).Take(3), options));
                            break;
                        case '2':
                            Console.WriteLine(warning);
                            Console.WriteLine(JsonSerializer.Serialize((await JsonSerializer.DeserializeAsync<IEnumerable<ProjectTaskDTO>>(await client.GetStreamAsync("Tasks/TasksByUser/" + MenuInputs.IdInput()), options)).Take(3), options));
                            break;
                        case '3':
                            Console.WriteLine(warning);
                            Console.WriteLine(JsonSerializer.Serialize((await JsonSerializer.DeserializeAsync<IEnumerable<FinishedTaskDTO>>(await client.GetStreamAsync("Tasks/FinishedTasks/" + MenuInputs.IdInput()), options)).Take(3), options));
                            break;
                        case '4':
                            Console.WriteLine(warning);
                            Console.WriteLine(pause);
                            Console.ReadKey();
                            Console.WriteLine(JsonSerializer.Serialize((await JsonSerializer.DeserializeAsync<IEnumerable<TeamMembersDTO>>(await client.GetStreamAsync("Teams/TeamMembers"), options)).Take(3), options));
                            break;
                        case '5':
                            Console.WriteLine(warning);
                            Console.WriteLine(pause);
                            Console.ReadKey();
                            Console.WriteLine(JsonSerializer.Serialize((await JsonSerializer.DeserializeAsync<IEnumerable<UserTasksDTO>>(await client.GetStreamAsync("Users/UserTasks"), options)).Take(3), options));
                            break;
                        case '6':
                            Console.WriteLine(JsonSerializer.Serialize(await JsonSerializer.DeserializeAsync<UserStatisticDTO>(await client.GetStreamAsync("Projects/UserStatistics/" + MenuInputs.IdInput()), options), options));
                            break;
                        case '7':
                            Console.WriteLine(warning);
                            Console.WriteLine(pause);
                            Console.ReadKey();
                            Console.WriteLine(JsonSerializer.Serialize((await JsonSerializer.DeserializeAsync<IEnumerable<ProjectStatisticDTO>>(await client.GetStreamAsync("Projects/ProjectStatistics"), options)).Take(3), options));
                            break;
                        case '8':
                            MarkRandom();
                            break;
                        case 'a':
                            await UserMenu.Menu(client, options);
                            break;
                        case 'b':
                            await TeamMenu.Menu(client, options);
                            break;
                        case 'c':
                            await taskMenu.Value.Menu();
                            break;
                        case 'd':
                            await ProjectMenu.Menu(client, options);
                            break;
                        case '0':
                            loop = false;
                            break;
                        default:
                            Console.WriteLine(menuItems);
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }
        public static async void MarkRandom()
        {
            try
            {
                Console.WriteLine("+Background job is set");
                var id = await taskMenu.Value.MarkRandomTaskWithDelay(1000);
                Console.WriteLine("+Background: {0} task marked finished", id);
            }
            catch (TaskCanceledException)
            {
                Console.WriteLine("+Background: No tasks to mark finished");
            }
            catch (Exception e)
            {
                Console.WriteLine("+Background: Exception :"+e.Message);
            }
        }
    }
}