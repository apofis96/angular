export interface UserUpdate {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    birthday: string;
    teamId?: number;
}