export interface TeamCreate {
    name: string;
    description: string;
}