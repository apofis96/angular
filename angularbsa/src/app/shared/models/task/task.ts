import { TaskState } from "./taskState";
import { User } from "../user/user";

export interface Task {
    id: number;
    name: string;
    description: string;
    createdAt: Date;
    finishedAt: Date;
    state: TaskState;
    projectId: number;
    performer: User;
}