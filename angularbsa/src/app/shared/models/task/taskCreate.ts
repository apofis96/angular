import { TaskState } from "./taskState";

export interface TaskCreate {
    name: string;
    description: string;
    finishedAt: Date;
    state: TaskState;
    projectId: number;
    performerId: number;
}