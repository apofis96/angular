import { TaskState } from "./taskState";

export interface TaskUpdate {
    id: number;
    name: string;
    description: string;
    finishedAt: string;
    state: TaskState;
    projectId: number;
    performerId: number;
}