export interface ProjectUpdate {
    id: number;
    name: string;
    description: string;
    deadline: string;
    authorId: number;
    teamId: number;
}