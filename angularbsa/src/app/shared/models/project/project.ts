import { Task } from "../task/task";
import { User } from "../user/user";
import { Team } from "../team/team";

export interface Project {
    id: number;
    name: string;
    description: string;
    createdAt: Date;
    deadline: Date;
    tasks: Task[]
    author: User;
    team: Team;
}
