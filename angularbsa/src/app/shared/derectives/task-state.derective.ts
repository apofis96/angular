import {Directive, ElementRef, Renderer2} from '@angular/core';
import { TaskState } from '../models/task/taskState';
 
@Directive({
    selector: '[taskState]'
})
export class TaskStateDirective {
    private taskState = TaskState;
    constructor(private elementRef: ElementRef, renderer: Renderer2) {}

    ngAfterViewInit() {
        switch(this.elementRef.nativeElement.innerText) { 
            case '0': { 
                this.elementRef.nativeElement.style.color = "blue"; 
               break; 
            } 
            case '1': { 
                this.elementRef.nativeElement.style.color = "cyan";  
               break; 
            }
            case '2': { 
                this.elementRef.nativeElement.style.color = "green"; 
                break; 
            }
            case '3': { 
                this.elementRef.nativeElement.style.color = "red"; 
                break; 
            }  
            default: { 
               break; 
            } 
         } 
        this.elementRef.nativeElement.innerText = this.taskState[this.elementRef.nativeElement.innerText];
    }
}