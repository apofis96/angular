import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ExitGuard, ComponentCanDeactivate} from './guards/exit.guard'
import {HttpInternalService} from './services/http-internal.service'
import { HttpClientModule } from '@angular/common/http';
import { TaskStateDirective } from './derectives/task-state.derective';
import { FormatDatePipe } from './pipes/formatDate.pipe';

@NgModule({
    declarations: [TaskStateDirective, FormatDatePipe],
    imports: [
        CommonModule,
        HttpClientModule
    ],
    exports: [TaskStateDirective, FormatDatePipe]
})
export class SharedModule { }
