import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({ providedIn: 'root' })
export class SnackBarService {
    public constructor(private snackBar: MatSnackBar) {}

    public showErrorMessage(error: any) {
        if(error.error.error)
        {
            this.snackBar.open(error.error.error, '', { duration: 3000, panelClass: 'error-snack-bar' });
        }
        else
        {
            this.snackBar.open(error.message, '', { duration: 3000, panelClass: 'error-snack-bar' });
        }
    }

    public showUsualMessage(message: any) {
        this.snackBar.open(message, '', { duration: 3000, panelClass: 'usual-snack-bar' });
    }
}