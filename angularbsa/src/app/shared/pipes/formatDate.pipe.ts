import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe, registerLocaleData } from '@angular/common';
import localeUa from '@angular/common/locales/uk';
registerLocaleData(localeUa, 'ua');

@Pipe({
    name: 'formatDate'
})
export class FormatDatePipe implements PipeTransform {
    private pipe = new DatePipe('ua');

    transform(value: Date, args?: any): string {
        return this.pipe.transform(value, 'dd MMMM yyyy');
    }
}