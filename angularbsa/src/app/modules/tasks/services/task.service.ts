import { Injectable } from '@angular/core';
import { HttpInternalService } from '../../../shared/services/http-internal.service';
import { Task } from '../../../shared/models/task/task';
import { TaskUpdate } from '../../../shared/models/task/taskUpdate';
import { TaskCreate } from '../../../shared/models/task/taskCreate';

@Injectable()
export class TaskService {
    public routePrefix = '/Tasks';

    constructor(private httpService: HttpInternalService) { }

    public getTasks() {
        return this.httpService.getFullRequest<Task[]>(`${this.routePrefix}`);
    }

    public createTask(task: TaskCreate) {
        return this.httpService.postFullRequest<Task>(`${this.routePrefix}`, task);
    }

    public updateTask(task: TaskUpdate) {
        return this.httpService.putFullRequest<Task>(`${this.routePrefix}`, task);
    }

    public deleteTask(task: Task) {
        return this.httpService.deleteFullRequest(`${this.routePrefix}/${task.id}`)
    }
}