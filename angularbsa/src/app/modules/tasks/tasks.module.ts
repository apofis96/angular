import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskListComponent } from './components/task-list/task-list.component';
import { TaskComponent } from './components/task/task.component';
import { TaskEditComponent } from './components/task-edit/task-edit.component';
import { TaskCreateComponent } from './components/task-create/task-create.component';
import { ExitGuard } from '../../shared/guards/exit.guard';
import { HttpInternalService } from '../../shared/services/http-internal.service';
import { TaskService } from './services/task.service';
import { TasksRoutingModule } from './tasks-routing.module';
import { MaterialComponentsModule } from '../../shared/material-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
    declarations: [TaskListComponent, TaskComponent, TaskEditComponent, TaskCreateComponent],
    imports: [
        CommonModule,
        TasksRoutingModule,
        MaterialComponentsModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule
    ],
    providers: [ExitGuard, HttpInternalService, TaskService]
})
export class TasksModule { }