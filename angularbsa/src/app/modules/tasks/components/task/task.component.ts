import { Component, OnInit, OnDestroy, Input, EventEmitter, Output } from '@angular/core';
import { Task } from 'src/app/shared/models/task/task';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { TaskService } from '../../services/task.service';
import { takeUntil } from 'rxjs/operators';
import { TaskEditComponent } from '../task-edit/task-edit.component';

@Component({
    selector: 'app-task',
    templateUrl: './task.component.html',
    styleUrls: ['./task.component.sass']
})
export class TaskComponent implements OnDestroy {
    @Input() public task: Task;

    private unsubscribe$ = new Subject<void>();
    public show = true;

    constructor(
        public dialog: MatDialog,
        private snackBarService: SnackBarService,
        private taskService: TaskService
    ) {}

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public deleteTask() {
        this.taskService.deleteTask(this.task)
            .pipe(takeUntil(this.unsubscribe$)).subscribe(
                (resp) => {
                    if (resp) {
                        this.show = false;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openDialog() {
        this.dialog.open(TaskEditComponent, {
            data : this.task
        });
    }
}