import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { TaskState } from 'src/app/shared/models/task/taskState';
import { TaskUpdate } from 'src/app/shared/models/task/taskUpdate';
import { Subject } from 'rxjs';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Task } from 'src/app/shared/models/task/task';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { TaskService } from '../../services/task.service';
import { takeUntil } from 'rxjs/operators';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-task-edit',
    templateUrl: './task-edit.component.html',
    styleUrls: ['./task-edit.component.sass']
})
export class TaskEditComponent implements OnInit, OnDestroy {
    public taskUpdate: TaskUpdate;
    private unsubscribe$ = new Subject<void>();
    public show = true;
    ranges= TaskState;
    private pipe = new DatePipe('en-US');
    constructor(
        @Inject(MAT_DIALOG_DATA) public task: Task,
        private dialogRef: MatDialogRef<TaskEditComponent>,
        private snackBarService: SnackBarService,
        private taskService: TaskService,
    ) {}

    public ngOnInit(): void {
        this.dialogRef.disableClose = true;
        this.taskUpdate = { 
            id: this.task.id,
            name: this.task.name,
            description: this.task.description,
            finishedAt: this.pipe.transform(this.task.finishedAt, 'yyyy-MM-ddThh:mm'),
            state: this.task.state,
            projectId: this.task.projectId,
            performerId: this.task.performer?.id
        }
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public close(dirty: boolean) {
        if(dirty) {
            if (confirm("Are you sure you want to leave this page?"))
                this.dialogRef.close()
        }
        else
            this.dialogRef.close()
    }
    public getKeys() { 
        return Object.keys(TaskState).reduce((arr, key) => {
            if (!arr.includes(key)) {
                arr.push(TaskState[key]);
            }
            return arr;
        }, []); 
    }

    public updateTask() {
        this.taskService.updateTask(this.taskUpdate).pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.task.name = resp.body.name;
                        this.task.description = resp.body.description;
                        this.task.finishedAt = resp.body.finishedAt;
                        this.task.state = resp.body.state;
                        this.task.projectId = resp.body.projectId;
                        this.task.performer = resp.body.performer;
                        this.dialogRef.close()
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }
}