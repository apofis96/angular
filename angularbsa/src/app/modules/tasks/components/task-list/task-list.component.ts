import { Component, OnInit, OnDestroy } from '@angular/core';
import { ComponentCanDeactivate } from 'src/app/shared/guards/exit.guard';
import { Task } from 'src/app/shared/models/task/task';
import { Subject } from 'rxjs';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { TaskService } from '../../services/task.service';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-task-list',
    templateUrl: './task-list.component.html',
    styleUrls: ['./task-list.component.sass']
})
export class TaskListComponent implements OnInit, OnDestroy, ComponentCanDeactivate {
    public unsaved: boolean = false;
    public tasks: Task[] = [];
    private unsubscribe$ = new Subject<void>();
    public loading = false;
    public showCreate = false;
  
    constructor(private snackBarService: SnackBarService,
        private taskService: TaskService
    ) {}

    public ngOnInit(): void {
        this.getTasks()
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public canDeactivate(): boolean {
        if(this.unsaved) {
            return confirm("Are you sure you want to leave this page?");
        }
        return true;
    }

    public toggleCreateContainer() {
        this.showCreate = this.canDeactivate() ? !this.showCreate : true;
        if (!this.showCreate) this.unsaved = false;
    }

    public addCreatedTask(newTask : Task) {
        this.showCreate = false;
        this.tasks.push(newTask);
    }

    public getTasks() {
        this.loading = true;
        this.taskService.getTasks().pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.loading = false;
                    this.tasks = resp.body;
                },
                (error) => {
                    this.snackBarService.showErrorMessage(error);
                    this.loading = false;
                }
            );
    }
}