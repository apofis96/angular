import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { Task } from 'src/app/shared/models/task/task';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { TaskService } from '../../services/task.service';
import { takeUntil } from 'rxjs/operators';
import { TaskCreate } from 'src/app/shared/models/task/taskCreate';
import { TaskState } from 'src/app/shared/models/task/taskState';

@Component({
    selector: 'app-task-create',
    templateUrl: './task-create.component.html',
    styleUrls: ['./task-create.component.sass']
})
export class TaskCreateComponent implements OnInit, OnDestroy {
    @Output() onCreate = new EventEmitter<Task>();
    @Output() onEdit = new EventEmitter<boolean>();

    public taskForm : FormGroup;
    private unsubscribe$ = new Subject<void>();
    ranges= TaskState;
    
    constructor(
        private snackBarService: SnackBarService,
        private taskService: TaskService
    ) {}
  
    public ngOnInit(): void {
        this.taskForm = new FormGroup({
            "name": new FormControl("", [Validators.required, Validators.minLength(2), Validators.maxLength(100)]),
            "description": new FormControl("", [Validators.required, Validators.minLength(10), Validators.maxLength(500)]),
            "finishedAt": new FormControl("", Validators.required),
            "state": new FormControl("", Validators.required),
            "projectId": new FormControl("", Validators.required),
            "performerId": new FormControl("", Validators.required),
        });
        this.taskForm.valueChanges.subscribe(() => {
            this.onEdit.emit(this.taskForm.dirty);
        });
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
    public getKeys() { 
        return Object.keys(TaskState).reduce((arr, key) => {
            if (!arr.includes(key)) {
                arr.push(TaskState[key]);
            }
            return arr;
        }, []); 
    }

    public createTask(){
        this.taskService.createTask(<TaskCreate>this.taskForm.value)
            .pipe(takeUntil(this.unsubscribe$)).subscribe(
                (resp) => {
                    if (resp) {
                        this.snackBarService.showUsualMessage("Success!")
                        this.onCreate.emit(resp.body)
                        this.onEdit.emit(false);
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }
}