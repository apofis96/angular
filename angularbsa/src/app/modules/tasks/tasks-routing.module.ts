import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExitGuard } from '../../shared/guards/exit.guard';
import { TaskListComponent } from './components/task-list/task-list.component'

const tasksRoutes: Routes = [
    { path: 'tasks', component: TaskListComponent, canDeactivate: [ExitGuard] }
]

@NgModule({
    imports: [RouterModule.forChild(tasksRoutes)],
    exports: [RouterModule]
})
export class TasksRoutingModule { }
