import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamListComponent } from './components/team-list/team-list.component';
import { TeamComponent } from './components/team/team.component';
import { TeamsRoutingModule } from './teams-routing.module';
import { HttpInternalService } from '../../shared/services/http-internal.service';
import {SharedModule} from '../../shared/shared.module';
import { MaterialComponentsModule } from '../../shared/material-components.module';
import { ExitGuard } from '../../shared/guards/exit.guard';
import { TeamService } from './services/team.service';
import { TeamEditComponent } from './components/team-edit/team-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TeamCreateComponent } from './components/team-create/team-create.component';

@NgModule({
    declarations: [TeamListComponent, TeamComponent, TeamEditComponent, TeamCreateComponent],
    imports: [
        CommonModule,
        TeamsRoutingModule,
        MaterialComponentsModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule
    ],
    providers: [ExitGuard, HttpInternalService, TeamService]
})
export class TeamsModule { }