import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TeamListComponent} from './components/team-list/team-list.component'
import { ExitGuard } from '../../shared/guards/exit.guard';

const teamsRoutes: Routes = [
    { path: 'teams', component: TeamListComponent, canDeactivate: [ExitGuard]}
]

@NgModule({
    imports: [RouterModule.forChild(teamsRoutes)],
    exports: [RouterModule]
})
export class TeamsRoutingModule { }
