import { Component, OnInit, OnDestroy} from '@angular/core';
import { ComponentCanDeactivate } from '../../../../shared/guards/exit.guard';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TeamService } from '../../services/team.service';
import { SnackBarService } from '../../../../shared/services/snack-bar.service'
import { Team } from '../../../../shared/models/team/team';

@Component({
    selector: 'app-team-list',
    templateUrl: './team-list.component.html',
    styleUrls: ['./team-list.component.sass']
})
export class TeamListComponent implements OnInit, OnDestroy, ComponentCanDeactivate {
    public unsaved: boolean = false;
    public teams: Team[] = [];
    private unsubscribe$ = new Subject<void>();
    public loading = false;
    public showCreate = false;
  
    constructor(private snackBarService: SnackBarService,
        private teamService: TeamService
    ) {}

    public ngOnInit(): void {
        this.getTeams()
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public canDeactivate(): boolean {
        if(this.unsaved) {
            return confirm("Are you sure you want to leave this page?");
        }
        return true;
    }

    public toggleCreateContainer() {
        this.showCreate = this.canDeactivate() ? !this.showCreate : true;
        if (!this.showCreate) this.unsaved = false;
    }

    public addCreatedTeam(newTeam : Team) {
        this.showCreate = false;
        this.teams.push(newTeam);
    }

    public getTeams() {
        this.loading = true;
        this.teamService.getTeams().pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.loading = false;
                    this.teams = resp.body;
                },
                (error) => {
                    this.snackBarService.showErrorMessage(error);
                    this.loading = false;
                }
            );
    }
}