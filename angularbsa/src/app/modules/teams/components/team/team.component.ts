import { Component, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TeamService } from '../../services/team.service';
import { SnackBarService } from '../../../../shared/services/snack-bar.service'
import { Team } from '../../../../shared/models/team/team';
import { MatDialog } from '@angular/material/dialog';
import { TeamEditComponent } from '../team-edit/team-edit.component';

@Component({
    selector: 'app-team',
    templateUrl: './team.component.html',
    styleUrls: ['./team.component.sass']
})
export class TeamComponent implements OnDestroy {
    @Input() public team: Team;

    private unsubscribe$ = new Subject<void>();
    public show = true;

    constructor(
        public dialog: MatDialog,
        private snackBarService: SnackBarService,
        private teamService: TeamService
    ) {}

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public deleteTeam() {
        this.teamService.deleteTeam(this.team)
            .pipe(takeUntil(this.unsubscribe$)).subscribe(
                (resp) => {
                    if (resp) {
                        this.show = false;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openDialog() {
        this.dialog.open(TeamEditComponent, {
            data : this.team
        });
    }
}