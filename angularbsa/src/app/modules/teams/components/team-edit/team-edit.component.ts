import { Component, OnInit, OnDestroy, Input, Inject } from '@angular/core';
import { Team } from 'src/app/shared/models/team/team';
import { TeamUpdate } from 'src/app/shared/models/team/teamUpdate';
import { Subject } from 'rxjs';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { TeamService } from '../../services/team.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-team-edit',
    templateUrl: './team-edit.component.html',
    styleUrls: ['./team-edit.component.sass']
})
export class TeamEditComponent implements OnInit, OnDestroy {
    public teamUpdate: TeamUpdate;
    private unsubscribe$ = new Subject<void>();
    public show = true;

    constructor(
        @Inject(MAT_DIALOG_DATA) public team: Team,
        private dialogRef: MatDialogRef<TeamEditComponent>,
        private snackBarService: SnackBarService,
        private teamService: TeamService,
    ) {}

    public ngOnInit(): void {
        this.dialogRef.disableClose = true;
        this.teamUpdate = { id: this.team.id, name: this.team.name, description: this.team.description }
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public close(dirty: boolean) {
        if(dirty) {
            if (confirm("Are you sure you want to leave this page?"))
                this.dialogRef.close()
        }
        else
            this.dialogRef.close()
    }

    public updateTeam() {
        this.teamService.updateTeam(this.teamUpdate).pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.team.name = resp.body.name;
                        this.team.description = resp.body.description;
                        this.dialogRef.close()
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }
}