import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Team } from 'src/app/shared/models/team/team';
import { TeamCreate } from 'src/app/shared/models/team/teamCreate';
import { Subject } from 'rxjs';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { TeamService } from '../../services/team.service';
import { takeUntil } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'app-team-create',
    templateUrl: './team-create.component.html',
    styleUrls: ['./team-create.component.sass']
})
export class TeamCreateComponent implements OnInit, OnDestroy {
    @Output() onCreate = new EventEmitter<Team>();
    @Output() onEdit = new EventEmitter<boolean>();

    public teamForm : FormGroup;
    private unsubscribe$ = new Subject<void>();

    constructor(
        private snackBarService: SnackBarService,
        private teamService: TeamService
    ) {}

    public ngOnInit(): void {
        this.teamForm = new FormGroup({
            "name": new FormControl("", [Validators.required,
                Validators.minLength(2),
                Validators.maxLength(100)]),
            "description": new FormControl("", [Validators.required, 
                Validators.minLength(10),
                Validators.maxLength(500)]),
        });
        this.teamForm.valueChanges.subscribe(() => {
            this.onEdit.emit(this.teamForm.dirty);
        });
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public createTeam(){
        this.teamService.createTeam(<TeamCreate>this.teamForm.value)
            .pipe(takeUntil(this.unsubscribe$)).subscribe(
                (resp) => {
                    if (resp) {
                        this.snackBarService.showUsualMessage("Success!")
                        this.onCreate.emit(resp.body)
                        this.onEdit.emit(false);
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }
}