import { Injectable } from '@angular/core';
import { HttpInternalService } from '../../../shared/services/http-internal.service';
import { Team } from '../../../shared/models/team/team';
import { TeamUpdate } from '../../../shared/models/team/teamUpdate';
import { TeamCreate } from '../../../shared/models/team/teamCreate';

@Injectable()
export class TeamService {
    public routePrefix = '/Teams';

    constructor(private httpService: HttpInternalService) { }

    public getTeams() {
        return this.httpService.getFullRequest<Team[]>(`${this.routePrefix}`);
    }

    public createTeam(team: TeamCreate) {
        return this.httpService.postFullRequest<Team>(`${this.routePrefix}`, team);
    }

    public updateTeam(team: TeamUpdate) {
        return this.httpService.putFullRequest<Team>(`${this.routePrefix}`, team);
    }

    public deleteTeam(team: Team) {
        return this.httpService.deleteFullRequest(`${this.routePrefix}/${team.id}`)
    }
}