import { Component, OnInit, OnDestroy } from '@angular/core';
import { ComponentCanDeactivate } from 'src/app/shared/guards/exit.guard';
import { Project } from 'src/app/shared/models/project/project';
import { Subject } from 'rxjs';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { ProjectService } from '../../services/project.service';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-project-list',
    templateUrl: './project-list.component.html',
    styleUrls: ['./project-list.component.sass']
})
export class ProjectListComponent implements OnInit, OnDestroy, ComponentCanDeactivate {
    public unsaved: boolean = false;
    public projects: Project[] = [];
    private unsubscribe$ = new Subject<void>();
    public loading = false;
    public showCreate = false;

    constructor(private snackBarService: SnackBarService,
        private projectService: ProjectService
    ) {}

    public ngOnInit(): void {
        this.getProjects()
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public canDeactivate(): boolean {
        if(this.unsaved) {
            return confirm("Are you sure you want to leave this page?");
        }
        return true;
    }

    public toggleCreateContainer() {
        this.showCreate = this.canDeactivate() ? !this.showCreate : true;
        if (!this.showCreate) this.unsaved = false;
    }

    public addCreatedProject(newProject : Project) {
        this.showCreate = false;
        this.projects.push(newProject);
    }

    public getProjects() {
        this.loading = true;
        this.projectService.getProjects().pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.loading = false;
                    this.projects = resp.body;
                },
                (error) => {
                    this.snackBarService.showErrorMessage(error);
                    this.loading = false;
                }
            );
    }
}