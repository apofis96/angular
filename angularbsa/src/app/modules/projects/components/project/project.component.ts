import { Component, OnDestroy, Input } from '@angular/core';
import { Project } from 'src/app/shared/models/project/project';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { ProjectService } from '../../services/project.service';
import { takeUntil } from 'rxjs/operators';
import { ProjectEditComponent } from '../project-edit/project-edit.component';

@Component({
    selector: 'app-project',
    templateUrl: './project.component.html',
    styleUrls: ['./project.component.sass']
})
export class ProjectComponent implements OnDestroy {
    @Input() public project: Project;

    private unsubscribe$ = new Subject<void>();
    public show = true;

    constructor(
        public dialog: MatDialog,
        private snackBarService: SnackBarService,
        private projectService: ProjectService
    ) {}

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public deleteProject() {
        this.projectService.deleteProject(this.project)
            .pipe(takeUntil(this.unsubscribe$)).subscribe(
                (resp) => {
                    if (resp) {
                        this.show = false;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openDialog() {
        this.dialog.open(ProjectEditComponent, {
            data : this.project
        });
    }
}