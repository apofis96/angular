import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ProjectUpdate } from 'src/app/shared/models/project/projectUpdate';
import { Subject } from 'rxjs';
import { DatePipe } from '@angular/common';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Project } from 'src/app/shared/models/project/project';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { ProjectService } from '../../services/project.service';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-project-edit',
    templateUrl: './project-edit.component.html',
    styleUrls: ['./project-edit.component.sass']
})
export class ProjectEditComponent implements OnInit, OnDestroy {
    public projectUpdate: ProjectUpdate;
    private unsubscribe$ = new Subject<void>();
    public show = true;
    private pipe = new DatePipe('en-US');
    constructor(
        @Inject(MAT_DIALOG_DATA) public project: Project,
        private dialogRef: MatDialogRef<ProjectEditComponent>,
        private snackBarService: SnackBarService,
        private projectService: ProjectService,
    ) {}

    public ngOnInit(): void {
        this.dialogRef.disableClose = true;
        this.projectUpdate = { 
            id: this.project.id,
            name: this.project.name,
            description: this.project.description,
            deadline: this.pipe.transform(this.project.deadline, 'yyyy-MM-ddThh:mm'),
            authorId: this.project.author?.id,
        teamId: this.project.team?.id,
        }
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public close(dirty: boolean) {
        if(dirty) {
            if (confirm("Are you sure you want to leave this page?"))
                this.dialogRef.close()
        }
        else
            this.dialogRef.close()
    }

    public updateProject() {
        this.projectService.updateProject(this.projectUpdate).pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.project.name = resp.body.name;
                        this.project.description = resp.body.description;
                        this.project.deadline = resp.body.deadline;
                        this.project.author = resp.body.author;
                        this.project.team = resp.body.team;
                        this.dialogRef.close()
                  } 
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }
}