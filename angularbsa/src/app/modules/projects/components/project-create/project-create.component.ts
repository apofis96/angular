import { Component, OnInit, Output, OnDestroy, EventEmitter } from '@angular/core';
import { Project } from 'src/app/shared/models/project/project';
import { Subject } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { takeUntil } from 'rxjs/operators';
import { ProjectCreate } from 'src/app/shared/models/project/projectCreate';
import { ProjectService } from '../../services/project.service';

@Component({
    selector: 'app-project-create',
    templateUrl: './project-create.component.html',
    styleUrls: ['./project-create.component.sass']
})
export class ProjectCreateComponent implements OnInit, OnDestroy {
    @Output() onCreate = new EventEmitter<Project>();
    @Output() onEdit = new EventEmitter<boolean>();

    public projectForm : FormGroup;
    private unsubscribe$ = new Subject<void>();
    
    constructor(
        private snackBarService: SnackBarService,
        private projectService: ProjectService
    ) {}
  
    public ngOnInit(): void {
        this.projectForm = new FormGroup({
            "name": new FormControl("", [Validators.required, Validators.minLength(2), Validators.maxLength(100)]),
            "description": new FormControl("", [Validators.required, Validators.minLength(10), Validators.maxLength(500)]),
            "deadline": new FormControl("", Validators.required),
            "authorId": new FormControl("", Validators.required),
            "teamId": new FormControl("", Validators.required),
        });
        this.projectForm.valueChanges.subscribe(() => {
            this.onEdit.emit(this.projectForm.dirty);
        });
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public createProject(){
        this.projectService.createProject(<ProjectCreate>this.projectForm.value)
            .pipe(takeUntil(this.unsubscribe$)).subscribe(
                (resp) => {
                    if (resp) {
                        this.snackBarService.showUsualMessage("Success!")
                        this.onCreate.emit(resp.body)
                        this.onEdit.emit(false);
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }
}