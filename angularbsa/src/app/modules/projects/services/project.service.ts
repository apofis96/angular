import { Injectable } from '@angular/core';
import { HttpInternalService } from '../../../shared/services/http-internal.service';
import { Project } from '../../../shared/models/project/project';
import { ProjectCreate } from '../../../shared/models/project/projectCreate';
import { ProjectUpdate } from '../../../shared/models/project/projectUpdate';

@Injectable()
export class ProjectService {
    public routePrefix = '/Projects';

    constructor(private httpService: HttpInternalService) { }

    public getProjects() {
        return this.httpService.getFullRequest<Project[]>(`${this.routePrefix}`);
    }

    public createProject(project: ProjectCreate) {
        return this.httpService.postFullRequest<Project>(`${this.routePrefix}`, project);
    }

    public updateProject(project: ProjectUpdate) {
        return this.httpService.putFullRequest<Project>(`${this.routePrefix}`, project);
    }

    public deleteProject(project: Project) {
        return this.httpService.deleteFullRequest(`${this.routePrefix}/${project.id}`)
    }
}