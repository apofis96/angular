import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExitGuard } from '../../shared/guards/exit.guard';
import { ProjectListComponent } from './components/project-list/project-list.component'

const projectsRoutes: Routes = [
    { path: 'projects', component: ProjectListComponent, canDeactivate: [ExitGuard]}
]

@NgModule({
    imports: [RouterModule.forChild(projectsRoutes)],
    exports: [RouterModule]
})
export class ProjectsRoutingModule { }