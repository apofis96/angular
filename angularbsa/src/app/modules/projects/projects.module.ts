import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectComponent } from './components/project/project.component';
import { ProjectListComponent } from './components/project-list/project-list.component';
import { ProjectEditComponent } from './components/project-edit/project-edit.component';
import { ProjectCreateComponent } from './components/project-create/project-create.component';
import { ProjectsRoutingModule } from './projects-routing.module';
import { HttpInternalService } from '../../shared/services/http-internal.service';
import { ExitGuard } from '../../shared/guards/exit.guard';
import { ProjectService } from './services/project.service';
import { MaterialComponentsModule } from '../../shared/material-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
    declarations: [ProjectComponent, ProjectListComponent, ProjectEditComponent, ProjectCreateComponent],
    imports: [
        CommonModule,
        ProjectsRoutingModule,
        MaterialComponentsModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule
    ],
    providers: [ExitGuard, HttpInternalService, ProjectService]
})
export class ProjectsModule { }