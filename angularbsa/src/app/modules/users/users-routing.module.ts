import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserListComponent} from './components/user-list/user-list.component';
import { ExitGuard } from '../../shared/guards/exit.guard';

const usersRoutes: Routes = [
    { path: 'users', component: UserListComponent, canDeactivate: [ExitGuard]}
]

@NgModule({
    imports: [RouterModule.forChild(usersRoutes)],
    exports: [RouterModule]
})
export class UsersRoutingModule { }
