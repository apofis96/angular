import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from 'src/app/shared/models/user/user';
import { Subject } from 'rxjs';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { UserService } from '../../services/user.service';
import { UserCreate } from 'src/app/shared/models/user/userCreate';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-user-create',
    templateUrl: './user-create.component.html',
    styleUrls: ['./user-create.component.sass']
})
export class UserCreateComponent implements OnInit, OnDestroy {
    @Output() onCreate = new EventEmitter<User>();
    @Output() onEdit = new EventEmitter<boolean>();

    public userForm : FormGroup;
    private unsubscribe$ = new Subject<void>();

    constructor(
        private snackBarService: SnackBarService,
        private userService: UserService
    ) {}

    public ngOnInit(): void {
        this.userForm = new FormGroup({
            "firstName": new FormControl("", Validators.required),
            "lastName": new FormControl("", Validators.required),
            "email": new FormControl("", [Validators.required, Validators.email]),
            "birthday": new FormControl("", Validators.required),
            "teamId": new FormControl(),
        });
        this.userForm.valueChanges.subscribe(() => {
            this.onEdit.emit(this.userForm.dirty);
        });
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public createUser(){
        this.userService.createUser(<UserCreate>this.userForm.value)
            .pipe(takeUntil(this.unsubscribe$)).subscribe(
                (resp) => {
                    if (resp) {
                        this.snackBarService.showUsualMessage("Success!")
                        this.onCreate.emit(resp.body)
                        this.onEdit.emit(false);
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }
}