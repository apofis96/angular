import { Component, OnInit, OnDestroy } from '@angular/core';
import { ComponentCanDeactivate } from 'src/app/shared/guards/exit.guard';
import { User } from 'src/app/shared/models/user/user';
import { Subject } from 'rxjs';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { UserService } from '../../services/user.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.sass']
})
export class UserListComponent implements OnInit, OnDestroy, ComponentCanDeactivate {
    public unsaved: boolean = false;
    public users: User[] = [];
    private unsubscribe$ = new Subject<void>();
    public loading = false;
    public showCreate = false;
  
    constructor(private snackBarService: SnackBarService,
        private userService: UserService
    ) {}

    public ngOnInit(): void {
        this.getUsers()
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public canDeactivate(): boolean {
        if(this.unsaved) {
            return confirm("Are you sure you want to leave this page?");
        }
        return true;
    }

    public toggleCreateContainer() {
        this.showCreate = this.canDeactivate() ? !this.showCreate : true;
        if (!this.showCreate) this.unsaved = false;
    }

    public addCreatedUser(newUser : User) {
        this.showCreate = false;
        this.users.push(newUser);
    }

    public getUsers() {
        this.loading = true;
        this.userService.getUsers().pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.loading = false;
                    this.users = resp.body;
                },
                (error) => {
                    this.snackBarService.showErrorMessage(error);
                    this.loading = false;
                }
            );
    }
}

