import { Component, OnInit, Input, Output, OnDestroy, EventEmitter } from '@angular/core';
import { User } from 'src/app/shared/models/user/user';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { UserService } from '../../services/user.service';
import { takeUntil } from 'rxjs/operators';
import { UserEditComponent } from '../user-edit/user-edit.component';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.sass']
})
export class UserComponent implements OnDestroy {
    @Input() public user: User;

    private unsubscribe$ = new Subject<void>();
    public show = true;

    constructor(
        public dialog: MatDialog,
        private snackBarService: SnackBarService,
        private userService: UserService
    ) {}

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public deleteUser() {
        this.userService.deleteUser(this.user)
            .pipe(takeUntil(this.unsubscribe$)).subscribe(
                (resp) => {
                    if (resp) {
                        this.show = false;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openDialog() {
        this.dialog.open(UserEditComponent, {
            data : this.user
        });
    }
}