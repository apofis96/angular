import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { UserUpdate } from 'src/app/shared/models/user/userUpdate';
import { Subject } from 'rxjs';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { User } from 'src/app/shared/models/user/user';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { UserService } from '../../services/user.service';
import { takeUntil } from 'rxjs/operators';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-user-edit',
    templateUrl: './user-edit.component.html',
    styleUrls: ['./user-edit.component.sass']
})
export class UserEditComponent implements OnInit, OnDestroy {
    public userUpdate: UserUpdate;
    private unsubscribe$ = new Subject<void>();
    public show = true;
    private pipe = new DatePipe('en-US');


    constructor(
        @Inject(MAT_DIALOG_DATA) public user: User,
        private dialogRef: MatDialogRef<UserEditComponent>,
        private snackBarService: SnackBarService,
        private userService: UserService,
    ) {}

    public ngOnInit(): void {
        this.dialogRef.disableClose = true;
        this.userUpdate = { 
            id: this.user.id,
            firstName: this.user.firstName,
            lastName: this.user.lastName,
            email: this.user.email,
            birthday: this.pipe.transform(this.user.birthday, 'yyyy-MM-ddThh:mm'),
            teamId: this.user.teamId, 
        }
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public close(dirty: boolean) {
        if(dirty) {
            if (confirm("Are you sure you want to leave this page?"))
                this.dialogRef.close()
        }
        else
            this.dialogRef.close()
    }

    public updateUser() {
        this.userService.updateUser(this.userUpdate).pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.user.firstName = resp.body.firstName;
                        this.user.lastName = resp.body.lastName;
                        this.user.email = resp.body.email;
                        this.user.birthday = resp.body.birthday;
                        this.user.teamId = resp.body.teamId;
                        this.dialogRef.close()
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }
}
