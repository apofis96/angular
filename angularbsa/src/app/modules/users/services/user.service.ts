import { Injectable } from '@angular/core';
import { HttpInternalService } from '../../../shared/services/http-internal.service';
import { User } from '../../../shared/models/user/user';
import { UserUpdate } from '../../../shared/models/user/userUpdate';
import { UserCreate } from '../../../shared/models/user/userCreate';

@Injectable()
export class UserService {
    public routePrefix = '/Users';

    constructor(private httpService: HttpInternalService) { }

    public getUsers() {
        return this.httpService.getFullRequest<User[]>(`${this.routePrefix}`);
    }

    public createUser(user: UserCreate) {
        return this.httpService.postFullRequest<User>(`${this.routePrefix}`, user);
    }

    public updateUser(user: UserUpdate) {
        return this.httpService.putFullRequest<User>(`${this.routePrefix}`, user);
    }

    public deleteUser(user: User) {
        return this.httpService.deleteFullRequest(`${this.routePrefix}/${user.id}`)
    }
}