import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {  UserListComponent } from './components/user-list/user-list.component';
import {  UserComponent } from './components/user/user.component';
import {  UsersRoutingModule } from './users-routing.module';
import { HttpInternalService } from '../../shared/services/http-internal.service';
import {SharedModule} from '../../shared/shared.module';
import { MaterialComponentsModule } from '../../shared/material-components.module';
import { ExitGuard } from '../../shared/guards/exit.guard';
import {  UserService } from './services/user.service';
import {  UserEditComponent } from './components/user-edit/user-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {  UserCreateComponent } from './components/user-create/user-create.component';

@NgModule({
    declarations: [ UserListComponent,  UserComponent,  UserEditComponent, UserCreateComponent],
    imports: [
        CommonModule,
        UsersRoutingModule,
        MaterialComponentsModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule
    ],
    providers: [ExitGuard, HttpInternalService, UserService]
})
export class UsersModule { }
