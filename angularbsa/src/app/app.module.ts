import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { TeamsRoutingModule } from './modules/teams/teams-routing.module';
import { UsersRoutingModule } from './modules/users/users-routing.module';
import { TasksRoutingModule } from './modules/tasks/tasks-routing.module';
import { ProjectsRoutingModule } from './modules/projects/projects-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainComponent } from './components/main/main.component';
import { SharedModule } from './shared/shared.module';
import { MaterialComponentsModule } from './shared/material-components.module';
import { TeamsModule } from './modules/teams/teams.module';
import { UsersModule } from './modules/users/users.module';
import { TasksModule } from './modules/tasks/tasks.module';
import { ProjectsModule } from './modules/projects/projects.module';

@NgModule({
    declarations: [
        AppComponent,
        MainComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        TeamsRoutingModule,
        UsersRoutingModule,
        TasksRoutingModule,
        ProjectsRoutingModule,
        BrowserAnimationsModule,
        SharedModule,
        TeamsModule,
        MaterialComponentsModule,
        UsersModule,
        TasksModule,
        ProjectsModule,
    ],
    exports: [
        SharedModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
